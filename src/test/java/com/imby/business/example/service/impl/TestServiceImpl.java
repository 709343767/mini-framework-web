package com.imby.business.example.service.impl;

import com.imby.business.example.entity.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import com.imby.business.example.dao.ITestDao;
import com.imby.business.example.service.ITestService;

/**
 * <p>
 * 测试服务类
 * </p>
 *
 * @author 皮锋
 * @custom.date 2020/2/11 16:10
 */
@Service
public class TestServiceImpl implements ITestService {

    @Autowired
    private ITestDao testDao;

    @Override
    public int insert(Test test) {
        return this.testDao.insert(test);
    }

    @Override
    public Test selectOne(@NonNull Integer id) {
        return this.testDao.selectOne(id);
    }
}
