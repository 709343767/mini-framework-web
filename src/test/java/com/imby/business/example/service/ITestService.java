package com.imby.business.example.service;

import com.imby.business.example.entity.Test;
import org.springframework.lang.NonNull;

/**
 * <p>
 * 测试服务接口
 * </p>
 *
 * @author 皮锋
 * @custom.date 2020/2/11 16:08
 */
public interface ITestService {

    int insert(Test test);

    Test selectOne(@NonNull Integer id);
}
