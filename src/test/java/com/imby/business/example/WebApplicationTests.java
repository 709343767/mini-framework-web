package com.imby.business.example;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.imby.business.example.service.ITestService;

/**
 * <p>
 * 主测试类
 * </p>
 *
 * @author 皮锋
 * @custom.date 2020年2月11日 下午5:00:24
 */
@SpringBootTest
public class WebApplicationTests {

    @Autowired
    private ITestService testService;

    @Test
    void insert() {
        com.imby.business.example.entity.Test test = com.imby.business.example.entity.Test.builder().build();
        test.setId(1);
        test.setFzbryid(null);
        test.setFzbry("超级管理员");
        test.setFsbsj(null);
        test.setFxbsj(null);
        test.setFzbjl("㮾梨");
        test.setFzb(null);
        test.setFzbryid2("1");
        test.setFjjsx(null);
        int result = this.testService.insert(test);
        System.out.println(result);
    }

    @Test
    void selectOne() {
        com.imby.business.example.entity.Test test = this.testService.selectOne(1);
        String str = test.toString();
        System.out.println(str);
    }
}
