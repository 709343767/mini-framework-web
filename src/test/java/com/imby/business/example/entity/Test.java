package com.imby.business.example.entity;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * <p>
 * Test表
 * </p>
 *
 * @author 皮锋
 * @custom.date 2020/2/11 17:07
 */
@Getter
@Setter
@Builder
@EqualsAndHashCode(callSuper = false)
@ToString
@Accessors(chain = true)
@SuppressFBWarnings("EI_EXPOSE_REP2")
public class Test {

    private Integer id;

    private Integer fzbryid;

    private String fzbry;

    @SuppressFBWarnings("EI_EXPOSE_REP")
    private Date fsbsj;

    @SuppressFBWarnings("EI_EXPOSE_REP")
    private Date fxbsj;

    private String fzbjl;

    private String fzb;

    private String fzbryid2;

    private String fjjsx;
}
