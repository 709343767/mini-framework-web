package com.imby.business.example.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.imby.business.example.entity.Test;

/**
 * <p>
 * 测试数据层接口
 * </p>
 *
 * @author 皮锋
 * @custom.date 2020/2/11 16:11
 */
@Repository
public interface ITestDao {

    @Insert("insert into W_ZBJL (ID,F_ZBRY_ID,F_ZBRY,F_SBSJ,F_XBSJ,F_ZBJL,F_ZB,F_ZBRYID,F_JJSX) " +
            "VALUES (#{id, jdbcType=INTEGER},#{fzbryid, jdbcType=INTEGER},#{fzbry, jdbcType=VARCHAR}," +
            "#{fsbsj, jdbcType=DATE},#{fxbsj, jdbcType=DATE},#{fzbjl, jdbcType=NCHAR}," +
            "#{fzb,jdbcType=VARCHAR},#{fzbryid2, jdbcType=VARCHAR},#{fjjsx, jdbcType=VARCHAR})")
    int insert(Test test);

    @Select("SELECT ID as id,F_ZBRY_ID as fzbryid,F_ZBRY as fzbry,F_SBSJ as fsbsj,F_XBSJ as fxbsj,F_ZBJL as fzbjl,F_ZB as fzb,F_ZBRYID as fzbryid2,F_JJSX as fjjsx FROM W_ZBJL WHERE ID=#{id}")
    Test selectOne(@Param("id") Integer id);
}
