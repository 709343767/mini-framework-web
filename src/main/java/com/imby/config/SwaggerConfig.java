package com.imby.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * <p>
 * 配置Swagger，用于生成、描述、调用和可视化restful风格的web服务
 * </p>
 *
 * @author 皮锋
 * @custom.date 2019年10月30日 下午8:45:23
 */
@Configuration
@EnableSwagger2
@Slf4j
public class SwaggerConfig {

	/**
	 * Swagger要扫描的包路径
	 */
	private static final String BASE_PACKAGE = "com.imby.business";

	/**
	 * <p>
	 * 创建rest风格的Swagger api
	 * </p>
	 *
	 * @author 皮锋
	 * @custom.date 2020年1月20日 上午10:28:13
	 * @return Docket
	 */
	@Bean
	public Docket createRestApi() {
		Docket docket = new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select()
				// 包路径
				.apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))
				//
				.paths(PathSelectors.any())
				//
				.build();
		log.info("Swagger配置成功！");
		return docket;
		// return new Docket(DocumentationType.SWAGGER_2)//
		// .apiInfo(apiInfo())//
		// .select()//
		// .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))//
		// .build();
	}

	/**
	 * <p>
	 * 构建详细api文档信息，包括标题、版本号、描述
	 * </p>
	 *
	 * @author 皮锋
	 * @custom.date 2020年1月20日 上午9:58:47
	 * @return ApiInfo
	 */
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				// 页面标题
				.title("迷你web小框架")
				// 版本号
				.version("1.0.0")
				// 描述
				.description("自己搭建一个迷你的web小框架，为以后快速开启web项目做准备。")
				// 构建
				.build();
	}
}
