package com.imby.config;

import com.imby.business.example.job.TimingGetUserByUserIdJob;
import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>
 * quartz任务配置类
 * </p>
 *
 * @author 皮锋
 * @custom.date 2020年2月18日 上午8:54:20
 */
@Configuration
public class QuartzTaskConfig {

    /**
     * <p>
     * 创建JobDetail实例
     * </p>
     *
     * @return JobDetail
     * @author 皮锋
     * @custom.date 2020/2/17 23:08
     */
    @Bean
    public JobDetail userJobDetail() {
        return JobBuilder.newJob(TimingGetUserByUserIdJob.class).withIdentity(TimingGetUserByUserIdJob.class.getName()).storeDurably().build();
    }

    /**
     * <p>
     * Cron触发器定义与设置
     * </p>
     *
     * @return Cron触发器
     * @author 皮锋
     * @custom.date 2020/2/17 23:08
     */
    @Bean
    public CronTrigger timingGetUserByCronTrigger() {
        // Cron类型：通过cron表达式设置触发规则（每10分钟执行一次）
        CronScheduleBuilder csb = CronScheduleBuilder.cronSchedule("0 0/10 * * * ?")
                .withMisfireHandlingInstructionDoNothing();
        // 一个Trigger只对应一个Job，Schedule调度器调度Trigger执行对应的Job
        return TriggerBuilder.newTrigger().forJob(userJobDetail()).withSchedule(csb).startNow().build();
    }
}
