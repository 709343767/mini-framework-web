package com.imby.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 配置RestTemplate，RestTemplate是Spring提供的用于访问Rest服务的客户端，提供了多种便捷访问远程Http服务的方法，能够大大提高客户端的编写效率
 * </p>
 *
 * @author 皮锋
 * @custom.date 2020年1月20日 上午10:58:31
 */
@Configuration
@Slf4j
public class RestTemplateConfig {

	/**
	 * 注入RestTemplate构造器
	 */
	@Autowired
	RestTemplateBuilder restTemplateBuilder;

	/**
	 * <p>
	 * 构造RestTemplate实例，把RestTemplate实例作为一个JavaBean交给Spring管理
	 * </p>
	 *
	 * @author 皮锋
	 * @custom.date 2020年1月20日 上午11:05:40
	 * @return RestTemplate
	 */
	@Bean
	public RestTemplate restTemplate() {
		RestTemplate restTemplate = restTemplateBuilder.build();
		log.info("RestTemplate配置成功！");
		return restTemplate;
	}
}
