package com.imby.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

/**
 * <p>
 * Web日志切面，在生产环境中可把日志存入数据库
 * </p>
 *
 * @author 皮锋
 * @custom.date 2020/2/22 17:25
 */
@Aspect
@Component
@Slf4j
public class WebLogAspect {

    /**
     * 进入方法时间戳
     */
    private Long startTime;
    /**
     * 方法结束时间戳(计时)
     */
    private Long endTime;

    /**
     * <p>
     * 定义切入点，切入点为com.imby.business.*.controller下的所有函数
     * </p>
     *
     * @author 皮锋
     * @custom.date 2020/2/22 17:56
     */
    @Pointcut("execution(public * com.imby.business.*.controller..*.*(..))")
    public void webLog() {
    }

    /**
     * <p>
     * 前置通知
     * </p>
     * 1. 在执行目标方法之前执行，比如请求接口之前的登录验证；<br>
     * 2. 在前置通知中设置请求日志信息，如开始时间、求参数、注解内容等<br>
     *
     * @param joinPoint 提供对连接点上可用状态和有关状态的静态信息的反射访问
     * @author 皮锋
     * @custom.date 2020/2/22 18:01
     */
    @Before("webLog()")
    public void doBefore(JoinPoint joinPoint) {
        // 进入方法时间戳
        this.startTime = System.currentTimeMillis();
        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        assert attributes != null;
        HttpServletRequest request = attributes.getRequest();
        // 记录下请求内容
        log.info("请求url：{}", request.getRequestURL().toString());
        log.info("请求方式：{}", request.getMethod());
        log.info("请求ip：{}", request.getRemoteAddr());
        log.info("请求方法：{}", joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        log.info("请求参数：{}", Arrays.toString(joinPoint.getArgs()));
    }

    /**
     * <p>
     * 返回通知
     * </p>
     * 1. 在目标方法正常结束之后执行；<br>
     * 2. 在返回通知中补充请求日志信息，如返回时间、方法耗时、返回值，并且可保存日志信息<br>
     *
     * @param ret 目标方法正常结束之后的返回值
     * @author 皮锋
     * @custom.date 2020/2/22 19:10
     */
    @AfterReturning(returning = "ret", pointcut = "webLog()")
    public void doAfterReturning(Object ret) {
        // 方法结束时间戳(计时)
        this.endTime = System.currentTimeMillis();
        // 处理完请求，返回内容
        log.info("请求返回：{}", ret);
        log.info("请求耗时：{}ms", this.endTime - this.startTime);
    }

    /**
     * <p>
     * 异常通知
     * </p>
     * 1. 在目标方法非正常结束，发生异常或者抛出异常时执行；<br>
     * 1. 在异常通知中设置异常信息，并将其保存<br>
     *
     * @param throwable 异常信息
     * @author 皮锋
     * @custom.date 2020/2/22 19:53
     */
    @AfterThrowing(value = "webLog()", throwing = "throwable")
    public void doAfterThrowing(Throwable throwable) {
        // 方法结束时间戳(计时)
        this.endTime = System.currentTimeMillis();
        // 保存异常日志记录
        log.error("请求耗时：{}ms", this.endTime - this.startTime);
        log.error("发生异常时间：{}", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now()));
        log.error("抛出异常：{}", throwable.getMessage());
    }
}
