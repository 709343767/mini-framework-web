package com.imby.business.example.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.imby.business.example.dao.IUserDao;
import com.imby.business.example.dto.UserDto;
import com.imby.business.example.entity.User;
import com.imby.business.example.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户服务层对象
 * </p>
 *
 * @author 皮锋
 * @custom.date 2020年1月20日 下午3:43:23
 */
@Service
public class UserServiceImpl extends ServiceImpl<IUserDao, User> implements IUserService {

	/**
	 * 用户数据访问对象
	 */
	@Autowired
	private IUserDao userDao;

	/**
	 * <p>
	 * 获取所有用户
	 * </p>
	 *
	 * @return List 用户集合
	 * @author 皮锋
	 * @custom.date 2020年1月20日 下午4:12:10
	 */
	@Override
	public List<UserDto> getUsers() {
		// 要返回的dto集合
		List<UserDto> list = Lists.newArrayList();
		// 获取用户
		List<User> users = this.userDao.getUsers();
		for (User usr : users) {
			list.add(UserDto.builder().build().convertFor(usr));
		}
		return list;
	}

	/**
	 * <p>
	 * 根据用户id获取用户
	 * </p>
	 *
	 * @param id 用户id
	 * @return Optional
	 * @author 皮锋
	 * @custom.date 2020/2/10 11:53
	 */
	@Override
	public Optional<UserDto> getUser(@NonNull Integer id) {
		User user = this.userDao.getUser(id);
		UserDto userDto = UserDto.builder().build().convertFor(user);
		Optional<UserDto> userDtoOptional = Optional.fromNullable(userDto);
		// Optional<UserDto> userDtoOptional = Optional.of(userDto);
		return userDtoOptional;
	}

	/**
	 * <p>
	 * 分页获取用户，使用Mybatis-plus分页插件
	 * </p>
	 *
	 * @param pageNum  页码，默认为1
	 * @param pageSize 每页多少条，默认为10
	 * @param id       用户id
	 * @return List 用户集合
	 * @author 皮锋
	 * @custom.date 2020年2月10日 下午2:40:41
	 */
	@Override
	public List<UserDto> getUsersByPage(Integer pageNum, Integer pageSize, @Nullable Integer id) {
		// 请求条件
		Map<String, Object> m = new HashMap<>();
		m.put("id", id);
		Page<User> page = new Page<>(pageNum, pageSize);
		List<User> users = this.userDao.getUsersByPage(m, page);
		// 要返回的dto集合
		List<UserDto> list = Lists.newArrayList();
		for (User usr : users) {
			list.add(UserDto.builder().build().convertFor(usr));
		}
		return list;
	}

	/**
	 * <p>
	 * 分页获取用户，使用Mybatis的分页插件
	 * </p>
	 *
	 * @param pageNum  页码，默认为1
	 * @param pageSize 每页多少条，默认为10
	 * @param id       用户id
	 * @return List 用户集合
	 * @author 皮锋
	 * @custom.date 2020/2/12 10:36
	 */
	@Override
	public List<UserDto> getUsersByPage2(Integer pageNum, Integer pageSize, @Nullable Integer id) {
		PageHelper.startPage(pageNum, pageSize);
		List<User> users = this.userDao.getUsersByPage2(id);
		// 要返回的dto集合
		List<UserDto> list = Lists.newArrayList();
		for (User usr : users) {
			list.add(UserDto.builder().build().convertFor(usr));
		}
		return list;
	}

}
