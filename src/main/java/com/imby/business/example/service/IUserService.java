package com.imby.business.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.google.common.base.Optional;
import com.imby.business.example.dto.UserDto;
import com.imby.business.example.entity.User;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.List;

/**
 * <p>
 * 用户服务层接口对象
 * </p>
 *
 * @author 皮锋
 * @custom.date 2020年1月20日 下午3:42:38
 */
public interface IUserService extends IService<User> {

	/**
	 * <p>
	 * 获取所有用户
	 * </p>
	 *
	 * @return List 用户集合
	 * @author 皮锋
	 * @custom.date 2020年1月20日 下午4:12:10
	 */
	List<UserDto> getUsers();

	/**
	 * <p>
	 * 根据用户id获取用户
	 * </p>
	 *
	 * @param id 用户id
	 * @return Optional
	 * @author 皮锋
	 * @custom.date 2020/2/10 11:46
	 */
	Optional<UserDto> getUser(@NonNull Integer id);

	/**
	 * <p>
	 * 分页获取用户，使用Mybatis-plus分页插件
	 * </p>
	 *
	 * @param pageNum  页码，默认为1
	 * @param pageSize 每页多少条，默认为10
	 * @param id       用户id
	 * @return List 用户集合
	 * @author 皮锋
	 * @custom.date 2020年2月10日 下午2:40:41
	 */
	List<UserDto> getUsersByPage(Integer pageNum, Integer pageSize, @Nullable Integer id);

	/**
	 * <p>
	 * 分页获取用户，使用Mybatis的分页插件
	 * </p>
	 *
	 * @param pageNum  页码，默认为1
	 * @param pageSize 每页多少条，默认为10
	 * @param id       用户id
	 * @return List 用户集合
	 * @author 皮锋
	 * @custom.date 2020/2/12 10:36
	 */
	List<UserDto> getUsersByPage2(Integer pageNum, Integer pageSize, @Nullable Integer id);

}
