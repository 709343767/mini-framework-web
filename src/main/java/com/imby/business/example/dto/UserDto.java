package com.imby.business.example.dto;

import org.springframework.beans.BeanUtils;

import com.google.common.base.Converter;
import com.imby.business.example.entity.User;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户数据传输对象
 * </p>
 *
 * @author 皮锋
 * @custom.date 2020年1月20日 下午4:08:45
 */
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@Setter
@Getter
@Builder
public class UserDto {

	/**
	 * 用户名
	 */
	@ApiModelProperty(value = "用户名")
	@Excel(name = "用户名", orderNum = "1", width = 15)
	private String username;

	/**
	 * 年龄
	 */
	@ApiModelProperty(value = "年龄")
	@Excel(name = "年龄", orderNum = "2", width = 15)
	private Integer age;

	/**
	 * 用户id
	 */
	@ApiModelProperty(value = "ID")
	@Excel(name = "用户ID", orderNum = "0", width = 15)
	private Integer id;

	/**
	 * <p>
	 * UserDto转User
	 * </p>
	 *
	 * @return User
	 * @author 皮锋
	 * @custom.date 2020年1月20日 下午4:32:24
	 */
	public User convertToUser() {
		UserDtoConvert userDtoConvert = new UserDtoConvert();
		return userDtoConvert.convert(this);
	}

	/**
	 * <p>
	 * User转UserDto
	 * </p>
	 *
	 * @param user 用户
	 * @return UserDto
	 * @author 皮锋
	 * @custom.date 2020年1月20日 下午4:33:59
	 */
	public UserDto convertFor(User user) {
		UserDtoConvert userDtoConvert = new UserDtoConvert();
		return userDtoConvert.reverse().convert(user);
	}

	/**
	 * <p>
	 * UserDto转换器
	 * </p>
	 *
	 * @author 皮锋
	 * @custom.date 2020年1月20日 下午4:35:20
	 */
	private static class UserDtoConvert extends Converter<UserDto, User> {

		@Override
		protected User doForward(UserDto userDto) {
			User user = User.builder().build();
			BeanUtils.copyProperties(userDto, user);
			return user;
		}

		@Override
		protected UserDto doBackward(User user) {
			UserDto userDto = UserDto.builder().build();
			BeanUtils.copyProperties(user, userDto);
			return userDto;
			// throw new AssertionError("不支持逆向转化方法!");
		}
	}

}
