package com.imby.business.example.job;

import com.imby.business.example.dao.IUserDao;
import com.imby.business.example.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * <p>
 * 定时获取所有用户Job，测试spring Task作业调度框架
 * </p>
 *
 * @author 皮锋
 * @custom.date 2020/2/22 10:21
 */
@Component
@Slf4j
public class TimingGetAllUsersJob {

    /**
     * 用户数据访问对象
     */
    @Autowired
    private IUserDao userDao;

    /**
     * <p>
     * 定时获取所有用户，每5分钟执行一次
     * </p>
     *
     * @author 皮锋
     * @custom.date 2020/2/22 10:25
     */
    @Scheduled(cron = "0 0/5 * * * ?")
    public void timingGetAllUsers() {
        // 获取所有用户
        List<User> users = this.userDao.getUsers();
        log.info("测试spring Task作业调度框架，定时执行！！！---{}",
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now()));
        users.forEach(user -> log.info("用户信息：{}", user.toString()));
    }

}
