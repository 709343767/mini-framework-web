package com.imby.business.example.job;

import com.imby.business.example.dao.IUserDao;
import com.imby.business.example.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * <p>
 * 定时根据用户id获取用户Job，测试quartz开源作业调度框架
 * </p>
 *
 * @author 皮锋
 * @custom.date 2020年2月17日 下午4:05:09
 */
@Component
@Slf4j
public class TimingGetUserByUserIdJob extends QuartzJobBean {

    /**
     * 用户数据访问对象
     */
    @Autowired
    private IUserDao userDao;

    /**
     * <p>
     * 执行Job
     * </p>
     *
     * @param context Job上下文，包含各种环境信息的句柄，它在执行时被赋予org.quartz.JobDetail实例，在执行完成后被赋予触发器实例
     * @throws JobExecutionException Job执行异常
     * @author 皮锋
     * @custom.date 2020/2/17 23:15
     */
    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        //用户id
        Integer userId = 1;
        // 定时根据用户id获取用户
        this.timingGetUser(userId);
    }

    /**
     * <p>
     * 定时根据用户id获取用户，每10分钟执行一次
     * </p>
     *
     * @param userId 用户id
     * @throws JobExecutionException Job执行异常
     * @author 皮锋
     * @custom.date 2020年2月18日 上午9:09:50
     */
    private void timingGetUser(Integer userId) throws JobExecutionException {
        try {
            // 根据用户id获取用户
            User user = this.userDao.getUser(userId);
            log.info("测试quartz开源作业调度框架，定时执行！！！---{}，用户信息：{}",
                    DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now()), user.toString());
        } catch (Exception e) {
            throw new JobExecutionException("定时根据用户id获取用户失败！", e.getCause());
        }
    }

}
