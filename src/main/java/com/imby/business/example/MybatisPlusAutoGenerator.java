package com.imby.business.example;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.VelocityTemplateEngine;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * <p>
 * MyBatis-Plus代码生成器
 * </p>
 * <ul><li>1.在不同的包路径下生成代码，只需拷贝此类到包路径下，并修改类中父包路径（SUPER_PACKAGE）和父文件路径（SUPER_FILE_PATH）为对应路径</li>
 * <li>2.不同的数据库信息只需要修改属性：URL、DB_TYPE、DRIVER_NAME、USERNAME、PASSWORD</li></ul>
 *
 * @author 皮锋
 * @custom.date 2020/2/13 11:14
 */
public class MybatisPlusAutoGenerator {

    /**
     * 父包路径
     */
    private static final String SUPER_PACKAGE = "com.imby.business.example";

    /**
     * 父文件路径
     */
    private static final String SUPER_FILE_PATH = "src/main/java/com/imby/business/example";

    /**
     * 数据库连接URL
     */
    private static final String URL = "jdbc:oracle:thin:@10.43.1.76:1525:gstest";

    /**
     * 数据库类型
     */
    private static final DbType DB_TYPE = DbType.ORACLE;

    /**
     * 数据库驱动类名
     */
    private static final String DRIVER_NAME = "oracle.jdbc.OracleDriver";

    /**
     * 数据库连接用户名
     */
    private static final String USERNAME = "bpm";

    /**
     * 数据库连接密码
     */
    private static final String PASSWORD = "bpm$2018";

    /**
     * <p>
     * 读取控制台内容
     * </p>
     *
     * @return 表名，多个表名用英文逗号分割
     * @author 皮锋
     * @custom.date 2020/2/13 12:49
     */
    @SuppressWarnings("resource")
    private static String scanner() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8));
        Scanner scanner = new Scanner(br);
        System.out.println("请输入表名，多个表名用英文逗号分割：");
        if (scanner.hasNext()) {
            String ipt = scanner.next();
            if (StringUtils.isNotEmpty(ipt)) {
                return ipt;
            }
        }
        throw new MybatisPlusException("请输入正确的表名，多个表名用英文逗号分割！");
    }

    /**
     * <p>
     * 主方法，生成代码
     * </p>
     *
     * @param args 参数数组
     * @author 皮锋
     * @custom.date 2020/2/13 12:53
     */
    public static void main(String[] args) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");
        gc.setOutputDir(projectPath + "/src/main/java");
        //作者
        gc.setAuthor(System.getenv().get("USERNAME"));
        //是否展开包
        gc.setOpen(false);
        // 是否覆盖已有文件
        gc.setFileOverride(true);
        // 实体属性Swagger2注解
        gc.setSwagger2(true);
        // 自定义文件命名，注意 %s会自动填充表实体属性
        gc.setMapperName("I%sDao");
        gc.setServiceName("I%sService");
        gc.setServiceImplName("%sServiceImpl");
        gc.setControllerName("%sController");
        mpg.setGlobalConfig(gc);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl(URL);
        dsc.setDbType(DB_TYPE);
        dsc.setDriverName(DRIVER_NAME);
        dsc.setUsername(USERNAME);
        dsc.setPassword(PASSWORD);
        mpg.setDataSource(dsc);

        // 包配置
        PackageConfig pc = new PackageConfig();
        // pc.setModuleName(scanner("模块名"));
        pc.setParent(SUPER_PACKAGE);
        pc.setEntity("entity");
        pc.setController("controller");
        pc.setService("service");
        pc.setServiceImpl("service.impl");
        pc.setMapper("dao");
        mpg.setPackageInfo(pc);

        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
            }
        };
        List<FileOutConfig> focList = new ArrayList<>();
        focList.add(new FileOutConfig("/templates/mapper.xml.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输入文件名称
                return projectPath + "/" + SUPER_FILE_PATH + "/mapper/" +
                        tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
            }
        });
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        // 配置模板
        TemplateConfig templateConfig = new TemplateConfig();
        // 不需要其他的类型时，直接设置为null就不会成对应的模版了
        // templateConfig.setEntity(null);
        // templateConfig.setService(null);
        // templateConfig.setController(null);
        // templateConfig.setServiceImpl(null);
        templateConfig.setXml(null);
        mpg.setTemplate(templateConfig);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        strategy.setEntityTableFieldAnnotationEnable(true);
        //strategy.setSuperEntityClass(SUPER_PACKAGE + ".entity.BaseEntity");
        strategy.setEntityLombokModel(true);
        strategy.setEntitySerialVersionUID(false);
        strategy.setRestControllerStyle(true);
        //strategy.setSuperControllerClass(SUPER_PACKAGE + ".controller.BaseController");
        // 写于父类中的公共字段
        // strategy.setSuperEntityColumns("id");
        strategy.setInclude(scanner().split(","));
        // 驼峰转连字符
        strategy.setControllerMappingHyphenStyle(true);
        // strategy.setTablePrefix(pc.getModuleName() + "_");
        mpg.setStrategy(strategy);
        mpg.setTemplateEngine(new VelocityTemplateEngine());

        mpg.execute();
    }
}
