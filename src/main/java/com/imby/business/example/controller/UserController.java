package com.imby.business.example.controller;

import com.google.common.base.Optional;
import com.imby.business.example.dto.UserDto;
import com.imby.business.example.service.IUserService;
import com.imby.util.ExcelUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * <p>
 * 用户控制层对象
 * </p>
 *
 * @author 皮锋
 * @custom.date 2020年1月20日 下午3:45:29
 */
@Api(tags = "用户")
@RestController
@RequestMapping("/user")
public class UserController {

    /**
     * 用户服务层接口对象
     */
    @Autowired
    private IUserService userService;

    /**
     * <p>
     * 获取所有用户
     * </p>
     *
     * @return List 用户集合
     * @author 皮锋
     * @custom.date 2020年1月20日 下午4:11:28
     */
    @ApiOperation(value = "获取所有用户", notes = "查询")
    @GetMapping("/getUsers")
    public List<UserDto> getUsers() {
        return this.userService.getUsers();
    }

    /**
     * <p>
     * 根据用户id获取用户，有可能是缺省值
     * </p>
     *
     * @param id 用户id
     * @return UserDto 用户Dto，此实体有可能是缺省值
     * @author 皮锋
     * @custom.date 2020/2/10 12:15
     */
    @ApiOperation(value = "根据用户id获取用户", notes = "查询")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "id", value = "用户id", required = true, paramType = "query", dataType = "int")})
    @GetMapping("/getUser")
    public UserDto getUser(@RequestParam Integer id) {
        Optional<UserDto> optional = this.userService.getUser(id);
        return optional.or(UserDto.builder().build());
    }

    /**
     * <p>
     * 分页获取用户，使用Mybatis-plus分页插件
     * </p>
     *
     * @param pageNum  页码，默认为1
     * @param pageSize 每页多少条，默认为10
     * @param id       用户id
     * @return List 用户集合
     * @author 皮锋
     * @custom.date 2020年2月10日 下午2:36:31
     */
    @ApiOperation(value = "分页获取用户，使用Mybatis-plus分页插件", notes = "查询")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "pageNum", value = "页码", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "每页多少条", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "id", value = "用户id", paramType = "query", dataType = "int")})
    @GetMapping("/getUsersByPage")
    public List<UserDto> getUsersByPage(@RequestParam(defaultValue = "1") Integer pageNum,
                                        @RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(required = false) Integer id) {
        return this.userService.getUsersByPage(pageNum, pageSize, id);
    }

    /**
     * <p>
     * 分页获取用户，使用Mybatis分页插件
     * </p>
     *
     * @param pageNum  页码，默认为1
     * @param pageSize 每页多少条，默认为10
     * @param id       用户id
     * @return List 用户集合
     * @author 皮锋
     * @custom.date 2020/2/12 10:36
     */
    @ApiOperation(value = "分页获取用户，使用Mybatis分页插件", notes = "查询")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "pageNum", value = "页码", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "每页多少条", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "id", value = "用户id", paramType = "query", dataType = "int")})
    @GetMapping("/getUsersByPage2")
    public List<UserDto> getUsersByPage2(@RequestParam(defaultValue = "1") Integer pageNum,
                                         @RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(required = false) Integer id) {
        return this.userService.getUsersByPage2(pageNum, pageSize, id);
    }

    /**
     * <p>
     * 导出所有用户信息到excel文件
     * </p>
     *
     * @param response HttpServletResponse对象
     * @author 皮锋
     * @custom.date 2020年2月18日 下午6:19:54
     */
    @ApiOperation(value = "导出所有用户信息到excel文件", notes = "导出")
    @GetMapping("/exportAllUsers")
    @SneakyThrows
    public void exportAllUsers(HttpServletResponse response) {
        List<UserDto> list = this.userService.getUsers();
        String fileName = "用户信息" + DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS").format(LocalDateTime.now());
        ExcelUtils.exportExcel(list, "用户信息表", "用户信息", UserDto.class, fileName, response);
    }
}
