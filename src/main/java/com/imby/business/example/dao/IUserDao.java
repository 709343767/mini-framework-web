package com.imby.business.example.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imby.business.example.entity.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户数据访问对象
 * </p>
 *
 * @author 皮锋
 * @custom.date 2020年1月20日 下午3:35:07
 */
@Repository
public interface IUserDao extends BaseMapper<User> {

    /**
     * <p>
     * 获取所有用户
     * </p>
     *
     * @return List 用户集合
     * @author 皮锋
     * @custom.date 2020年1月20日 下午4:12:10
     */
    List<User> getUsers();

    /**
     * <p>
     * 根据用户id获取用户
     * </p>
     *
     * @param id 用户id
     * @return User 用户实体
     * @author 皮锋
     * @custom.date 2020/2/10 11:46
     */
    User getUser(@NonNull @Param("id") Integer id);

    /**
     * <p>
     * 分页获取用户，使用Mybatis-plus分页插件
     * </p>
     *
     * @param m    请求条件
     * @param page 页面请求对象
     * @return List 用户集合
     * @author 皮锋
     * @custom.date 2020年2月10日 下午2:51:12
     */
    List<User> getUsersByPage(@Param("m") Map<String, Object> m, Page<User> page);

    /**
     * <p>
     * 分页获取用户，使用Mybatis的分页插件
     * </p>
     *
     * @param id 用户id
     * @return List 用户集合
     * @author 皮锋
     * @custom.date 2020/2/12 10:42
     */
    List<User> getUsersByPage2(@Param("id") Integer id);
}
