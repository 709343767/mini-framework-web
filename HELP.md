#入门

###参考文件
为了进一步参考，请考虑以下部分：

*[官方Apache Maven文档](https://maven.apache.org/guides/index.html)  
*[Spring Boot Maven插件参考指南](https://docs.spring.io/spring-boot/docs/2.2.3.RELEASE/maven-plugin/)  
*[Spring Web](https://docs.spring.io/spring-boot/docs/2.2.3.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications)  
*[Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/2.2.3.RELEASE/reference/htmlsingle/#using-boot-devtools)  

###指南
以下指南说明如何具体使用某些功能：

*[构建RESTful Web服务](https://spring.io/guides/gs/rest-service/)  
*[使用Spring MVC提供Web内容](https://spring.io/guides/gs/serving-web-content/)  
*[使用Spring构建REST服务](https://spring.io/guides/tutorials/bookmarks/)  
*[使用mybatis-plus](https://mp.baomidou.com/)  
*[使用MyBatis分页插件PageHelper](https://pagehelper.github.io/)  
*[使用joda-time](https://www.ibm.com/developerworks/cn/java/j-jodatime.html)  
*[使用lombok](https://projectlombok.org/)  
*[使用poi](https://poi.apache.org/)
*[使用easy poi](https://opensource.afterturn.cn/doc/easypoi.html)  
