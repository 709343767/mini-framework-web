#（一）项目介绍
本项目为自己搭建的一个迷你web小框架，为以后快速开启web项目做准备。
* 开发语言：JAVA
* 开发工具：intellij idea 或者 eclipse
* 管理工具：maven
* 运行环境：JDK版本>=1.8
* 可执行文件：jar或者war

#（二）项目特点
本项目以Spring Boot 2.x为基础，集成web项目的常用依赖包，并且提供默认配置，旨在于快速开启一个简单的web企业级项目。
* 核心技术  
以Spring Boot为核心，spring MVC做web框架、mybatis-plus做持久层框架、druid做数据库连接池、undertow Web做嵌入式服务器，而构建的web项目。
* 集成框架  
1.undertow Web嵌入式服务器（也可以替换成Tomcat或者Jetty）  
2.spring MVC web框架  
3.lombok  
4.swagger（用于生成、描述、调用和可视化 RESTful 风格的 Web 服务）  
5.commons、guava、fastjson、findbugs等工具包  
6.mybatis-plus持久层框架（包括分页插件、SQL执行效率插件、代码生成器）  
7.druid数据库连接池  
8.poi、easy poi（用Java操作Microsoft Office的相关文件，但是一般我们都是用来操作Excel相关文件）  
9.joda-time时间日期框架  
10.mybatis-plus的分页插件和mybatis的pagehelper分页插件  
11.quartz（quartz是一个完全由Java编写的开源作业调度框架，本项目既可以使用quartz，也可以使用spring Task）  

#（三）项目的基本结构	 
com.imby.business.example                 示例包  
com.imby.business.example.constant        公共常量包  
com.imby.business.example.controller      控制层包  
com.imby.business.example.dao	          数据访问层对象接口包  
com.imby.business.example.dto	          数据传输层对象包  
com.imby.business.example.entity	      数据库实体对象包  
com.imby.business.example.job	          quartz作业包   
com.imby.business.example.mapper	      mapper文件包  
com.imby.business.example.service	      服务层包  
com.imby.business.example.service.impl	  服务层实现类包   
com.imby.config	                          配置包  
com.imby.util	                          工具包  

example包及其子包为本项目示例，自己的业务需要在example同级新建业务包及其子包，写在自己的业务包下，不能写在example包下。

#（四）使用方法
* swagger  
1.swagger的配置在com.imby.config.SwaggerConfig，可以配置扫描包路径，文档的标题、版本、描述等信息；  
2.swagger 文档地址：http://IP:PORT/mini-framework-web/swagger-ui.html  
* druid监控  
1.druid数据库连接池支持开启监控，可以通过配置spring.datasource.druid.web-stat-filter.enabled=true开启；  
2.druid监控地址：http://IP:PORT/mini-framework-web/druid/index.html  
* 注释和Java Doc  
1.自定义注释  
@custom.date 时间  
2.生成Java Doc  
生成Java Doc，需要添加命令行参数：-encoding UTF-8 -charset UTF-8 -tag custom.date:a:时间

#（五）混淆
1.com.google.common.base.Optional与java.util.Optional可以完成相同的功能，在JDK版本>=1.8时可以直接使用java.util.Optional，但是它们之间的方法有些差异。

#（六）关于作者/组织及交流方式
* 皮锋 709343767@qq.com 中通服创发科技有限责任公司

#（七）贡献者/贡献组织
* 皮锋 709343767@qq.com 中通服创发科技有限责任公司

#（八）鸣谢
* 感谢所有使用本项目的人
* 感谢所有为本项目做出贡献的人

#（九）版权信息
